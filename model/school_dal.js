var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT *FROM school;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(school_id, callback) {
    var query = 'SELECT c.*, a.street, a.zip_code FROM school c ' +
        'LEFT JOIN school_address ca on ca.school_id = c.school_id ' +
        'LEFT JOIN address a on a.address_id = ca.school_id ' +
        'WHERE c.school_id = ?';
    var queryData = [school_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE school
    var query = 'INSERT INTO school (school_name) VALUES (?)';

    var queryData = [params.school_name];

    connection.query(query, params.school_name, function(err, result) {

        // THEN USE THE school_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO school_ADDRESS
        var school_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO school (school_name) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var schoolNameData = [];
        if (params.school_name.constructor === Array) {
            for (var i = 0; i < params.school_name.length; i++) {
                schoolNameData.push([school_name]);
            }
        }
        else {
            schoolNameData.push([school_name]);
        }

        // NOTE THE EXTRA [] AROUND schoolAddressData
        connection.query(query, [schoolNameData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(school_name, callback) {
    var query = 'DELETE FROM school WHERE school_name = ?';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};



exports.update = function(params, callback) {
    var query = 'UPDATE school SET school_id = ? WHERE school_name = ?';
    var queryData = [params.school_name, params.school_id];
}


exports.edit = function(school_name, callback) {
    var query = 'CALL school_getinfo(?)';
    var queryData = [school_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

